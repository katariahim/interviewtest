package interviewTest;

import java.util.ArrayList;
import java.util.Scanner;

public class FirstNPrimeNumbers {

	//Method to get First N prime Numbers and store it in a Arraylist
	//Here I have used parameterization in method to ore handy
	ArrayList<Integer> getFirstNPrimeNumbers(int countOfPrimeNumbers)
	{
		int numToCheck =2, flag=0, count=0;
		ArrayList<Integer> listOfFirstNPrimeNumbers = new ArrayList<>();
		outer: while(true)
		{
			flag=0;
				for(int j=2; j<numToCheck;j++)
				{
					if(numToCheck%j==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{
					listOfFirstNPrimeNumbers.add(numToCheck);
					count++;
				}
				if(count==countOfPrimeNumbers)
				{
					break outer;
				}
			numToCheck++;
		}
		return listOfFirstNPrimeNumbers;
	}
	
	public static void main(String[] args)
	{
		FirstNPrimeNumbers fnpn = new FirstNPrimeNumbers();
		System.out.println(fnpn.getFirstNPrimeNumbers(100));
	}
}
